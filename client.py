# -*- coding: utf-8 -*-
import json
import logging
import sys
import time
from watchdog.observers import Observer

import paho.mqtt.client as mqtt
import pychromecast
from pychromecast.controllers import media

VERSION = "0.1"

MQTT_SERVER = "192.168.0.10"
MQTT_PORT = "1883"
MQTT_TOPIC = "media/streamer/action"

CONFIG_DIR = "config"
CONFIG_FILE = "config.json"

settings = {}

def watch_settings(path):
	observer = Observer()
	observer.schedule(read_config, path, recursive=False)
	observer.start()

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, rc):
	print("Connected with result code " + str(rc))
	# Subscribing in on_connect() means that if we lose the connection and
	# reconnect then subscriptions will be renewed.
	client.subscribe(MQTT_TOPIC)

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
	action = msg.payload.split(' ')[0]
	movie = " ".join(msg.payload.split(' ')[1:])

	print 'Action {0} movie {1}'.format(action, movie)

	if action == 'PLAY':
		play_movie(movie)
	elif action == 'STOP':
		print 'STOP'

def play_movie(movie_id):
	if not movie_id in settings['movies']:
		return False

	movies = settings['movies']

	cast = pychromecast.get_chromecast(friendly_name=settings['chromecast'])
	if not cast is None:
		media_controller = media.MediaController()
		cast.register_handler(media_controller)
		movie_path = movies[movie_id]['path']
		if movie_path.startswith("http://"):
			print 'Playing movie {1} ({2}) to Chromecast {0}'.format(settings['chromecast'], movie_path, movie_id)
			media_controller.play_media(movies[movie_id]['path'], movies[movie_id]['content_type'])
		else:
			movie_url = "http://{0}:{1}/{2}".format(settings["http_host"], settings["http_port"], movie_path)
			print 'Playing movie {1} ({2}) to Chromecast {0}'.format(settings['chromecast'], movie_url, movie_id)
			media_controller.play_media(movie_url, movies[movie_id]['content_type'])

	else:
		print 'Chromecast {0} not found'.format(settings['chromecast'])

def read_config(path):
	json_data = open(CONFIG_DIR + '/' + path).read()

	global settings
	settings = json.loads(json_data)

	print 'Number of movies in db: ', len(settings['movies'])

print 'MQTT Media Server Client Version ', VERSION

print 'Reading config'
watch_settings(CONFIG_DIR)
read_config(CONFIG_FILE)

print 'Subscribing to MQTT broker'
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect(MQTT_SERVER, MQTT_PORT, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
#client.loop_start()
client.loop_forever()
